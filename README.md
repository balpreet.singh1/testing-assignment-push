# Jenkins Assignment 3
Create a Scripted CI  pipeline for java based project that contains various stages like
Code checkout
Run below stages in parallel
- Code stability.
- Code quality analysis.
- Code coverage analysis.
Generate a report for code quality & analysis.
Publish artifacts.
Send Slack and Email notifications.
The user should have the option to skip various scans in the build execution. And before publish there should be an approval stage to be set in place to approve or deny the publish and if approved the step should execute and the user should be notified post successful/failed.


# Scripted Pipeline for Jenkins Assignment 3
![image](screenshots/Screenshot8.png)


# The above pipline produces the following output using blueocean

![image](screenshots/Screenshot1.png)


# .war Artifact Generated

![image](screenshots/Screenshot2.png)


# Cobertura Report

![image](screenshots/Screenshot5.png)

![image](screenshots/Screenshot4.png)


# Checkstyle Warnings

![image](screenshots/Screenshot7.png)

![image](screenshots/Screenshot6.png)


# Slack Notification

![image](screenshots/Screenshot3.png)

# Email Notification

![image](screenshots/Screenshot9.png)

